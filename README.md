# Labelme 数据增强
水平翻转、垂直翻转、增加exposure、减少exposure、加盐、高斯模糊
## Usage
```bash
python data_aug.py [img_dir] [output_dir]
```

### 参数说明
1. img_dir: 包含图片和对应labelme文件的文件夹
2. output_dir: 数据增强输出文件夹（不包含原始文件）