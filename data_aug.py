#import base64
import json
#from labelme import utils
import cv2 as cv
import sys
import numpy as np
import random
import re
import ntpath
import os
import tqdm


class DataAugment(object):
    def __init__(self, image_id=1, output_dir="./"):
        self.add_saltNoise = True
        self.gaussianBlur = True
        self.changeExposure = True
        self.id = image_id
        self.output_dir = output_dir
        img = cv.imread(str(self.id)+'.jpg')
        self.fname = ntpath.basename(self.id)
        try:
            img.shape
        except:
            print('No Such image!---'+str(id)+'.jpg')
            sys.exit(0)
        self.src = img
        dst1 = cv.flip(img, 0, dst=None)
        dst2 = cv.flip(img, 1, dst=None)
        dst3 = cv.flip(img, -1, dst=None)
        self.flip_x = dst1
        self.flip_y = dst2
        self.flip_x_y = dst3
        cv.imwrite(os.path.join(self.output_dir, self.fname+'_flip_x'+'.jpg'), self.flip_x)
        cv.imwrite(os.path.join(self.output_dir, self.fname+'_flip_y'+'.jpg'), self.flip_y)
        cv.imwrite(os.path.join(self.output_dir, self.fname+'_flip_x_y'+'.jpg'), self.flip_x_y)

    def gaussian_blur_fun(self):
        if self.gaussianBlur:
            dst1 = cv.GaussianBlur(self.src, (5, 5), 0)
            dst2 = cv.GaussianBlur(self.flip_x, (5, 5), 0)
            dst3 = cv.GaussianBlur(self.flip_y, (5, 5), 0)
            dst4 = cv.GaussianBlur(self.flip_x_y, (5, 5), 0)
            cv.imwrite(os.path.join(self.output_dir, self.fname+'_Gaussian'+'.jpg'), dst1)
            cv.imwrite(os.path.join(self.output_dir, self.fname+'_flip_x'+'_Gaussian'+'.jpg'), dst2)
            cv.imwrite(os.path.join(self.output_dir, self.fname+'_flip_y'+'_Gaussian'+'.jpg'), dst3)
            cv.imwrite(os.path.join(self.output_dir, self.fname+'_flip_x_y'+'_Gaussian'+'.jpg'), dst4)

    def change_exposure_fun(self):
        if self.changeExposure:
            # contrast
            reduce = 0.25
            increase = 2.5
            # brightness
            g = 3
            h, w, ch = self.src.shape

            add = np.zeros([h, w, ch], self.src.dtype)
            dst1 = cv.addWeighted(self.src, reduce, add, 1-reduce, g)
            dst2 = cv.addWeighted(self.src, increase, add, 1-increase, g)
            dst3 = cv.addWeighted(self.flip_x, reduce, add, 1 - reduce, g)
            dst4 = cv.addWeighted(self.flip_x, increase, add, 1 - increase, g)
            dst5 = cv.addWeighted(self.flip_y, reduce, add, 1 - reduce, g)
            dst6 = cv.addWeighted(self.flip_y, increase, add, 1 - increase, g)
            dst7 = cv.addWeighted(self.flip_x_y, reduce, add, 1 - reduce, g)
            dst8 = cv.addWeighted(self.flip_x_y, increase, add, 1 - increase, g)
            cv.imwrite(os.path.join(self.output_dir, self.fname+'_ReduceEp'+'.jpg'), dst1)
            cv.imwrite(os.path.join(self.output_dir, self.fname+'_flip_x'+'_ReduceEp'+'.jpg'), dst3)
            cv.imwrite(os.path.join(self.output_dir, self.fname+'_flip_y'+'_ReduceEp'+'.jpg'), dst5)
            cv.imwrite(os.path.join(self.output_dir, self.fname+'_flip_x_y'+'_ReduceEp'+'.jpg'), dst7)
            cv.imwrite(os.path.join(self.output_dir, self.fname+'_IncreaseEp'+'.jpg'), dst2)
            cv.imwrite(os.path.join(self.output_dir, self.fname+'_flip_x'+'_IncreaseEp'+'.jpg'), dst4)
            cv.imwrite(os.path.join(self.output_dir, self.fname+'_flip_y'+'_IncreaseEp'+'.jpg'), dst6)
            cv.imwrite(os.path.join(self.output_dir, self.fname+'_flip_x_y'+'_IncreaseEp'+'.jpg'), dst8)

    def add_salt_noise(self):
        if self.add_saltNoise:
            percentage = 0.01
            dst1 = self.src
            dst2 = self.flip_x
            dst3 = self.flip_y
            dst4 = self.flip_x_y
            num = int(percentage * self.src.shape[0] * self.src.shape[1])
            for i in range(num):
                rand_x = random.randint(0, self.src.shape[0] - 1)
                rand_y = random.randint(0, self.src.shape[1] - 1)
                if random.randint(0, 1) == 0:
                    dst1[rand_x, rand_y] = 0
                    dst2[rand_x, rand_y] = 0
                    dst3[rand_x, rand_y] = 0
                    dst4[rand_x, rand_y] = 0
                else:
                    dst1[rand_x, rand_y] = 255
                    dst2[rand_x, rand_y] = 255
                    dst3[rand_x, rand_y] = 255
                    dst4[rand_x, rand_y] = 255
            cv.imwrite(os.path.join(self.output_dir,self.fname+'_Salt'+'.jpg'), dst1)
            cv.imwrite(os.path.join(self.output_dir,self.fname+'_flip_x'+'_Salt'+'.jpg'), dst2)
            cv.imwrite(os.path.join(self.output_dir,self.fname+'_flip_y'+'_Salt'+'.jpg'), dst3)
            cv.imwrite(os.path.join(self.output_dir,self.fname+'_flip_x_y'+'_Salt'+'.jpg'), dst4)

    def json_generation(self):
        image_names = [self.fname+'_flip_x', self.fname+'_flip_y', self.fname+'_flip_x_y']
        if self.gaussianBlur:
            image_names.append(self.fname+'_Gaussian')
            image_names.append(self.fname+'_flip_x'+'_Gaussian')
            image_names.append(self.fname+'_flip_y' + '_Gaussian')
            image_names.append(self.fname+'_flip_x_y'+'_Gaussian')
        if self.changeExposure:
            image_names.append(self.fname+'_ReduceEp')
            image_names.append(self.fname+'_flip_x'+'_ReduceEp')
            image_names.append(self.fname+'_flip_y'+'_ReduceEp')
            image_names.append(self.fname+'_flip_x_y'+'_ReduceEp')
            image_names.append(self.fname+'_IncreaseEp')
            image_names.append(self.fname+'_flip_x'+'_IncreaseEp')
            image_names.append(self.fname+'_flip_y'+'_IncreaseEp')
            image_names.append(self.fname+'_flip_x_y'+'_IncreaseEp')
        if self.add_saltNoise:
            image_names.append(self.fname+'_Salt')
            image_names.append(self.fname+'_flip_x' + '_Salt')
            image_names.append(self.fname+'_flip_y' + '_Salt')
            image_names.append(self.fname+'_flip_x_y' + '_Salt')
        for image_name in image_names:

            with open(str(self.id)+".json", 'r')as js:
                json_data = json.load(js)
                # img = utils.img_b64_to_arr(json_data['imageData'])
                img = cv.imread(os.path.join(self.output_dir, image_name + '.jpg'))
                height, width = img.shape[:2]
                shapes = json_data['shapes']
                for shape in shapes:
                    points = shape['points']
                    for point in points:
                        match_pattern2 = re.compile(r'(.*)_x(.*)')
                        match_pattern3 = re.compile(r'(.*)_y(.*)')
                        match_pattern4 = re.compile(r'(.*)_x_y(.*)')
                        if match_pattern4.match(image_name):
                            point[0] = width - point[0]
                            point[1] = height - point[1]
                        elif match_pattern3.match(image_name):
                            point[0] = width - point[0]
                            point[1] = point[1]
                        elif match_pattern2.match(image_name):
                            point[0] = point[0]
                            point[1] = height - point[1]
                        else:
                            point[0] = point[0]
                            point[1] = point[1]


                    if match_pattern4.match(image_name):
                        pass
                    elif match_pattern2.match(image_name) or match_pattern3.match(image_name):
                        points.reverse()
                json_data['imagePath'] = image_name+".jpg"
                # json_data['imageData'] = base64_data
                json_data['imageData'] = None
                json.dump(json_data, open(os.path.join(self.output_dir, image_name+".json"), 'w'), indent=2)


if __name__ == "__main__":
    import argparse
    import ntpath
    import os
    import glob

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('img_dir', type=str)
    args_parser.add_argument('output_dir', type=str)
    args = args_parser.parse_args()

    images = glob.glob(os.path.join(args.img_dir, '*.jpg'))
    os.makedirs(args.output_dir, exist_ok=True)
    
    for img_path in tqdm.tqdm(images):
        name = img_path.rsplit('.', 1)[0]
        dataAugmentObject = DataAugment(name, args.output_dir)
        dataAugmentObject.gaussian_blur_fun()
        dataAugmentObject.change_exposure_fun()
        dataAugmentObject.add_salt_noise()
        dataAugmentObject.json_generation()



