#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""

import sys
import os
import cv2
import numpy as np
import random
import time
import json
import base64
import glob
import ntpath

from math import cos ,sin ,pi,fabs,radians


#读取json
def readJson(jsonfile):
    with open(jsonfile,encoding='utf-8') as f:
        jsonData = json.load(f)
    return jsonData

def rotate_bound(image, angle):
    """
    旋转图像
    :param image: 图像
    :param angle: 角度
    :return: 旋转后的图像
    """
    h, w,_ = image.shape
    # print(image.shape)
    (cX, cY) = (w // 2, h // 2)
    print(cX,cY)

    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))
    # print(nW,nH)
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY
    # print( M[0, 2], M[1, 2])
    image_rotate = cv2.warpAffine(image, M, (nW, nH),borderValue=(255,255,255))
    return image_rotate,cX,cY,angle


def dumpRotateImage(img, degree):
    height, width = img.shape[:2]
    heightNew = int(width * fabs(sin(radians(degree))) + height * fabs(cos(radians(degree))))
    widthNew = int(height * fabs(sin(radians(degree))) + width * fabs(cos(radians(degree))))
    matRotation = cv2.getRotationMatrix2D((width // 2, height // 2), degree, 1)
    matRotation[0, 2] += (widthNew - width) // 2
    matRotation[1, 2] += (heightNew - height) // 2
    print(width // 2,height // 2)
    imgRotation = cv2.warpAffine(img, matRotation, (widthNew, heightNew), borderValue=(255, 255, 255))
    return imgRotation,matRotation


def rotate_xy(x, y, angle, cx, cy):
    """
    点(x,y) 绕(cx,cy)点旋转
    """
    # print(cx,cy)
    angle = angle * pi / 180
    x_new = (x - cx) * cos(angle) - (y - cy) * sin(angle) + cx
    y_new = (x - cx) * sin(angle) + (y - cy) * cos(angle) + cy
    return x_new, y_new


#转base64
def image_to_base64(image_np):
    image = cv2.imencode('.jpg', image_np)[1]
    image_code = str(base64.b64encode(image))[2:-1]
    return image_code

#坐标旋转
def rotatePoint(Srcimg_rotate,jsonTemp,M,imagePath):
    json_dict = {}
    for key, value in jsonTemp.items():
        if key=='imageHeight':
            json_dict[key]=Srcimg_rotate.shape[0]
            print('height',json_dict[key])
        elif key=='imageWidth':
            json_dict[key] = Srcimg_rotate.shape[1]
            print('width',json_dict[key])
        elif key=='imageData':
            json_dict[key] = image_to_base64(Srcimg_rotate)
        elif key=='imagePath':
            json_dict[key] = imagePath
        else:
            json_dict[key] = value
    for item in json_dict['shapes']:
        for key, value in item.items():
            if key == 'points':
                for item2 in range(len(value)):
                    pt1=np.dot(M,np.array([[value[item2][0]],[value[item2][1]],[1]]))
                    value[item2][0], value[item2][1] = pt1[0][0], pt1[1][0]
    return json_dict

#保存json
def writeToJson(filePath,data):
    fb = open(filePath,'w')
    fb.write(json.dumps(data,indent=2)) # ,encoding='utf-8'
    fb.close()

if __name__=='__main__':
    import argparse

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('images_dir', type=str)
    args_parser.add_argument('output_dir', type=str)
    args_parser.add_argument('--angle', default=270, type=int, help='rotate angle (counter-clockwise direction)')
    args = args_parser.parse_args()

    images_dir = args.images_dir
    output_dir = args.output_dir
    angel = args.angle
    images = glob.glob(os.path.join(images_dir, '*.jpg'))
    os.makedirs(output_dir, exist_ok=True)

    for i, image_path in enumerate(images):
        imageName=ntpath.basename(image_path).rsplit(".", 1)[0]+ "_a"
        json_path = image_path.rsplit('.', 1)[0] + '.json'

        print(imageName, json_path)

        Srcimg = cv2.imread(image_path)  ##########gai label1,label2
        jsonData = readJson(json_path)  ######## 读取json
        Srcimg_rotate,M = dumpRotateImage(Srcimg, angel)  ######逆时针选装270度
        jsonData2=rotatePoint(Srcimg_rotate,jsonData,M,imageName+".jpg")
        cv2.imwrite(os.path.join(output_dir, imageName+".jpg"),Srcimg_rotate)
        writeToJson(os.path.join(output_dir, imageName+".json"), jsonData2)
        print('ok')
